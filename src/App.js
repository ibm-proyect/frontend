import "./App.css";
import { useState } from "react";
import {
  Modal,
  Dropdown,
  DropdownButton,
  Card,
  Col,
  Row,
  Image,
} from "react-bootstrap";
import axios from "axios";

const App = () => {
  const [show, setShow] = useState(false);
  const [users, setUsers] = useState([]);

  const showModal = () => {
    axios.get("users").then((res) => {
      setUsers(res.data);
      setShow(true);
    });
  };

  return (
    <div>
      <DropdownButton className="m-4" title="Dropdown">
        <Dropdown.Item onClick={() => showModal()}>Show Modal</Dropdown.Item>
      </DropdownButton>

      <Modal show={show} onHide={() => setShow(false)} centered>
        <Modal.Header closeButton>
          <Modal.Title id="modal-test">Modal</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {users?.map((item, key) => (
            <Card className="mb-2" body key={key}>
              <Row>
                <Col md={3}>
                  <Image
                    style={{ height: "4.5em", border: "3px solid #cbd300" }}
                    className="rounded-circle shadow ml-3"
                    src={"https://picsum.photos/200/200?random=" + key}
                  />
                </Col>
                <Col md={9}>
                  <h5>{item.name}</h5>
                  <p>
                    {item.address.street +
                      " " +
                      item.address.suite +
                      " " +
                      item.address.city}
                  </p>
                </Col>
              </Row>
            </Card>
          ))}
        </Modal.Body>
      </Modal>
    </div>
  );
};

export default App;
